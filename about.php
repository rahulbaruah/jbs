<?php include('config/settings.php') ?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Mirrored from max-themes.net/demos/kingster/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 11:47:26 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jettwings Business School</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel='stylesheet' href='plugins/goodlayers-core/plugins/combine/style.css' type='text/css' media='all' />
    <link rel='stylesheet' href='plugins/goodlayers-core/include/css/page-builder.css' type='text/css' media='all' />
    <link rel='stylesheet' href='plugins/revslider/public/assets/css/settings.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/style-core.css' type='text/css' media='all' />
    <link rel='stylesheet' href='css/kingster-style-custom.css?v=<?php echo($kingster_style_custom_css_version); ?>' type='text/css' media='all' />

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700%2C400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CABeeZee%3Aregular%2Citalic&amp;subset=latin%2Clatin-ext%2Cdevanagari&amp;ver=5.0.3' type='text/css' media='all' />
    <style type="text/css">
        .gdlr-core-title-item-title,.gdlr-core-course-item-icon,.gdlr-core-course-item-id {
            color: #ec2f45!important;
        }
        .gdlr-core-body .gdlr-core-button.gdlr-core-button-gradient {
            background-color: #ffffff;
            background: -webkit-linear-gradient(#ffffff, #ffffff);
            background: -o-linear-gradient(#ffffff, #ffffff);
            background: -moz-linear-gradient(#ffffff, #ffffff);
            background: linear-gradient(#ffffff, #ffffff);
        }
        .gdlr-core-course-item.gdlr-core-course-style-list .gdlr-core-course-item-title:hover {
            color: #ec2f45;
        }
        #more {display: none;}
        .right-sub-box {
            margin-top: 3em;
            margin-bottom: 3em;
        }
        .left-sub-box {
            margin-top: 3em;
            margin-bottom: 3em;
        }
        .right-box-list {
            margin-left: 0;
        }
        .right-box-list li {
            list-style: none;
        }
    </style>
</head>

<body class="home page-template-default page page-id-2039 gdlr-core-body woocommerce-no-js tribe-no-js kingster-body kingster-body-front kingster-full  kingster-with-sticky-navigation  kingster-blockquote-style-1 gdlr-core-link-to-lightbox">
    
    <?php include('mobile-header.php') ?>
    
    <div class="kingster-body-outer-wrapper ">
        <div class="kingster-body-wrapper clearfix  kingster-with-frame">
            
            <?php include('top-bar.php') ?>
            
            <?php include('header.php') ?>

            <div class="kingster-page-wrapper" id="kingster-page-wrapper">
                
                <?php include('about-content.php') ?>
                
            </div>

            <?php include('footer.php') ?>
            
        </div>
    </div>


	<script type='text/javascript' src='js/jquery/jquery.js'></script>
    <script type='text/javascript' src='js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='plugins/goodlayers-core/plugins/combine/script.js'></script>
    <script type='text/javascript' src='plugins/goodlayers-core/include/js/page-builder.js'></script>
    <script type='text/javascript' src='js/jquery/ui/effect.min.js'></script>
    <script type='text/javascript' src='js/plugins.min.js'></script>
</body>

<!-- Mirrored from max-themes.net/demos/kingster/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Aug 2019 11:47:26 GMT -->
</html>