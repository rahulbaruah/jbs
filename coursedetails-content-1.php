<div class="kingster-page-wrapper" id="kingster-page-wrapper" style="padding-top:30px">
                <div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">BBA in Aviation Management</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Academics</a></li>
        <li class="breadcrumb-item"><a href="#">BBA Degree-Diploma Programs</a></li>
        <li class="breadcrumb-item active" aria-current="page">BBA in Aviation Management</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
                    
    <div class=" kingster-sidebar-wrap clearfix kingster-line-height-0 kingster-sidebar-style-right">
                        
                        <div class=" kingster-sidebar-center kingster-column-40 kingster-line-height">
                            <div class="gdlr-core-page-builder-body">
                                <div class="gdlr-core-pbf-wrapper" style="padding-top:0">
                                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                            
<div class="card" style="background-color:#ec2f45;color:#ffffff">
  <div class="card-body">
    <p class="card-text">
        <span class="col-6" style="padding-left:0"><b>Course Id:</b> JW154</span>
        <span class="col-6" style="padding-left:0"><b>Duration:</b> 3 Years</span>
        <br/>
        <b>Eligibility:</b> 10+2 pass/appeared from a recognized board
        <br/>
        <b>Mode of Delivery:</b> Full-time, credit-hour system and semester-based
        <br/>
        <b>Certification:</b> Gauhati University, Approved by Govt. of Assam
    </p>
  </div>
</div>

<div>
<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">About the Aviation Management Program</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class="text-danger">Aviation Management is a unique specialization that is offerred by only a few universities around the world and Gauhati University is the only State University that offers BBA-Aviation in Association with Jettwings. The course is tailored to meet the needs of the job market in the fast-growing Indian & Global Aviation Industry.</p> 
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>

<div>
<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">Why enroll on this Program?</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class="text-danger">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div> 

<div>
<div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-top: 40px ;">
                                                    <div class="gdlr-core-title-item-title-wrap clearfix">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title text-danger" style="font-size: 20px ;font-weight: 600 ;">Career Prospects</h3></div>
                                                </div>
                                            </div>
<div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
        <p class="text-danger">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
        <p><b>The Aviation management course prepares students for professional careers in the following areas:</b></p>
        
        <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb clearfix " style="padding-bottom: 25px ;">
                                                    <ul>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airline Management
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airline flight operations
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airline ground operations
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airport management
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airport operations
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Airport planning
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Aviation consultancy firms
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Aviation charter firms
                                                            </span></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
        
</div> 

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class=" kingster-sidebar-right kingster-column-20 kingster-line-height kingster-line-height">
                            <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Other BBA Programs</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <ul>
                                                            <li>BBA in Hospitality Management</li>
                                                            <li>BBA in Tourism Management</li>
                                                            <li>BBA in Aviation, Hospitality & Tourism Management</li>
                                                        </ul>
                                        </div>
                                </div>
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Quick Links</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <ul>
                                                            <li>Scholarships</li>
                                                            <li>Admission Process</li>
                                                            <li>Apply Online</li>
                                                        </ul>
                                        </div>
                                </div>
                                
                                <div id="text-18" class="widget widget_text kingster-widget">
                                    <div class="textwidget">
                                        <div class="gdlr-core-widget-box-shortcode " style="color: #ffffff ;padding: 30px 45px;background-color: #ec2f45 ;">
                                            <div class="gdlr-core-widget-box-shortcode-content">
                                                </p>
                                                <h3 style="font-size: 20px; color: #fff; margin-bottom: 25px;">Contact Info</h3>
                                                <p><span style="font-size: 15px;">Nabagraha Road, Chenikuthi
                                                    <br /> Hill Side, Silpukhuri<br /> Gauhati - 781003</span></p>
                                                <p><span style="font-size: 15px;"><i class="fa fa-phone" aria-hidden="true"></i> (+91) 99540-90900<br /><i class="fa fa-envelope-o" aria-hidden="true"></i> info@jettwingsbschool.com<br /> </span></p>
                                                <p><span style="font-size: 16px;"><i class="fa fa-clock-o" aria-hidden="true"></i> Mon &#8211; Sat 9:00A.M. &#8211; 5:00P.M.</span></p> <span class="gdlr-core-space-shortcode" style="margin-top: 40px ;"></span>
                                                <span class="gdlr-core-space-shortcode" style="margin-top: 40px ;"></span> <a class="gdlr-core-button gdlr-core-button-shortcode  gdlr-core-button-gradient gdlr-core-button-no-border" href="#" style="padding: 16px 27px 18px;margin-right: 20px;border-radius: 2px;-webkit-border-radius: 2px;"><span class="gdlr-core-content" style="color:#111">Apply Now</span></a>
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>       
                
    </div>
</div>