<div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Our Expertise</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">About</a></li>
        <li class="breadcrumb-item active" aria-current="page">Our Expertise</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            
            <div class="left-sub-box">
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
            </div>
            
            <div class="left-sub-box">
                <img src="https://via.placeholder.com/450x530/ec2f45/ffffff"></img>
            </div>
        </div>
        <div class="col-md-8">
            
            <div class="right-sub-box">
            <p>The Core Focus Area of JBS is Top of the Line Placements The career development process includes self-awareness, career exploration, and job placement. The prime focus of JBS’s are:</p>
            </div>
            
            
            
        </div>
    </div>

</div>
                    
<div class="gdlr-core-pbf-wrapper " style="padding: 65px 0px 60px 0px;margin-top:50px">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #eeeeee ;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 45px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px ;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 23px ;text-transform: none ;color: #000000 ;">
                                                        <p>Through JBS we would like to promote a new-aged education that will result in a special breed of professionals from a multi-cultural ethnicity and prepare them for diversified global work culture. 
<br/>
<b>So, come be a part of a richer learning experience</b>
</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #ec2f45 ;"><span class="gdlr-core-content" >Apply Now</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" id="gdlr-core-column-1">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="margin: -123px 0px 0px 0px;padding: 0px 0px 0px 40px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="http://via.placeholder.com/400x257/000000/ffffff" width="700" height="450"  alt="" /></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>