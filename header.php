<header class="kingster-header-wrap kingster-header-style-plain  kingster-style-menu-right kingster-sticky-navigation kingster-style-fixed" data-navigation-offset="75px">
                <div class="kingster-header-background"></div>
                <div class="kingster-header-container  kingster-container">
                    <div class="kingster-header-container-inner clearfix">
                        <div class="kingster-logo  kingster-item-pdlr">
                            <div class="kingster-logo-inner">
                                <a class="" href="/"><img src="images/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="kingster-navigation kingster-item-pdlr clearfix ">
                            <div class="kingster-main-menu" id="kingster-main-menu">
                                <ul id="menu-main-navigation-1" class="sf-menu">
                                    <li class="menu-item kingster-normal-menu"><a href="/">Home</a></li>
                                    
                                    <li class="menu-item menu-item-home menu-item-has-children kingster-normal-menu"><a href="#">About Us</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item" data-size="60"><a href="/about.php">About JBS</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Our Story</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Our Motto</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Our Expertise</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Awards & Accreditations</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Chairman Message</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item menu-item-home menu-item-has-children kingster-normal-menu"><a href="#">Academics</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item" data-size="60"><a href="/courses.php">Courses</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Placement</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Scholarship</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item menu-item-home menu-item-has-children kingster-normal-menu"><a href="#">Admission</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item" data-size="60"><a href="#">Admission Procedure</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Apply Now</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">UGAT</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Admission Timeline</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item menu-item-home menu-item-has-children kingster-normal-menu"><a href="#">Courses</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item" data-size="60"><a href="/coursedetails.php?id=1">Course 1</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 2</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 3</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 4</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 5</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 6</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 7</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 8</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 9</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 10</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 11</a></li>
                                            <li class="menu-item" data-size="60"><a href="#">Course 12</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item kingster-normal-menu"><a href="#">Campus Life</a></li>
                                    <li class="menu-item kingster-normal-menu"><a href="#">Placement</a></li>
                                    <li class="menu-item kingster-normal-menu"><a href="#">Contact Us</a></li>
                                </ul>
                                <div class="kingster-navigation-slide-bar" id="kingster-navigation-slide-bar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>