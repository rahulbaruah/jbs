<div class="kingster-mobile-menu"><a class="kingster-mm-menu-button kingster-mobile-menu-button kingster-mobile-button-hamburger" href="#kingster-mobile-menu"><span></span></a>
                        <div class="kingster-mm-menu-wrap kingster-navigation-font" id="kingster-mobile-menu" data-slide="right">
                            <ul id="menu-main-navigation" class="m-menu">
                                <li class="menu-item"><a href="/">Home</a></li>
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">About Us</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="/about.php">About JBS</a></li>
                                        <li class="menu-item"><a href="#">Our Story</a></li>
                                        <li class="menu-item"><a href="#">Our Motto</a></li>
                                        <li class="menu-item"><a href="#">Our Expertise</a></li>
                                        <li class="menu-item"><a href="#">Awards & Accreditations</a></li>
                                        <li class="menu-item"><a href="#">Chairman Message</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">Academics</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="/courses.php">Courses</a></li>
                                        <li class="menu-item"><a href="#">Placement</a></li>
                                        <li class="menu-item"><a href="#">Scholarship</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">Admission</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="#">Admission Procedure</a></li>
                                        <li class="menu-item"><a href="#">Apply Now</a></li>
                                        <li class="menu-item"><a href="#">UGAT</a></li>
                                        <li class="menu-item"><a href="#">Admission Timeline</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-home menu-item-has-children"><a href="#">Courses</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-home"><a href="#">Course 1</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 2</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 3</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 4</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 5</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 6</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 7</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 8</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 9</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 10</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 11</a></li>
                                        <li class="menu-item menu-item-home"><a href="#">Course 12</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="#">Campus Life</a></li>
                                <li class="menu-item"><a href="#">Placement</a></li>
                                <li class="menu-item"><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>