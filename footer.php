<footer>
                <div class="kingster-footer-wrapper ">
                    <div class="kingster-footer-container kingster-container clearfix">
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="text-2" class="widget widget_text kingster-widget">
                                <div class="textwidget">
                                    <p><img src="images/logo.png" alt="" />
                                        <br /> <span class="gdlr-core-space-shortcode" style="margin-top: 5px ;"></span>
                                        <br /> Nabagraha Road, Chenikuthi
                                        <br /> Hill Side, Silpukhuri
                                        <br /> Gauhati - 781003</p>
                                    <p><span style="font-size: 15px; color: #ffffff;"><i class="fa fa-phone"></i> (+91) 99540-90900</span>
                                        <br /> <span class="gdlr-core-space-shortcode" style="margin-top: -20px ;"></span>
                                        <br /> <a style="font-size: 15px; color: #ffffff;" href="mailto:info@jettwingsbschool.com"><i class="fa fa-envelope"></i> info@jettwingsbschool.com</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="gdlr-core-custom-menu-widget-2" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                                <h3 class="kingster-widget-title">Quick Links</h3><span class="clear"></span>
                                <div class="menu-our-campus-container">
                                    <ul id="menu-our-campus" class="gdlr-core-custom-menu-widget gdlr-core-menu-style-plain">
                                        <li class="menu-item"><a href="#">Home</a></li>
                                        <li class="menu-item"><a href="#">Application Process</a></li>
                                        <li class="menu-item"><a href="#">Scholarship</a></li>
                                        <li class="menu-item"><a href="#">Placement</a></li>
                                        <li class="menu-item"><a href="#">Career</a></li>
                                        <li class="menu-item"><a href="#">Apply Now</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="gdlr-core-custom-menu-widget-3" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                                <h3 class="kingster-widget-title">Courses</h3><span class="clear"></span>
                                <div class="menu-campus-life-container">
                                    <ul id="menu-campus-life" class="gdlr-core-custom-menu-widget gdlr-core-menu-style-plain">
                                        <li class="menu-item"><a href="#">BBA Programs</a></li>
                                        <li class="menu-item"><a href="#">Post-Graduate Programs</a></li>
                                        <li class="menu-item"><a href="#">TISS-SVE Programs</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                            <div id="gdlr-core-custom-menu-widget-4" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                                <h3 class="kingster-widget-title">Quick Links</h3><span class="clear"></span>
                                <div class="menu-academics-container">
                                    <ul id="menu-academics" class="gdlr-core-custom-menu-widget gdlr-core-menu-style-plain">
                                        <li class="menu-item"><a href="#">Sitemap</a></li>
                                        <li class="menu-item"><a href="#">Terms & Conditions</a></li>
                                        <li class="menu-item"><a href="#">Scholarship</a></li>
                                        <li class="menu-item"><a href="#">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
				<div class="kingster-copyright-wrapper">
					<div class="kingster-copyright-container kingster-container clearfix">
						<div class="kingster-copyright-left kingster-item-pdlr">&copy; 2019, Jettwings Business School | All Right Reserved 2019</div>
						<div class="kingster-copyright-right kingster-item-pdlr">
							<div class="gdlr-core-social-network-item gdlr-core-item-pdb  gdlr-core-none-align" style="padding-bottom: 0px ;">
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="facebook">
									<i class="fa fa-facebook" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="google-plus">
									<i class="fa fa-google-plus" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="linkedin">
									<i class="fa fa-linkedin" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="skype">
									<i class="fa fa-skype" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="twitter">
									<i class="fa fa-twitter" ></i>
								</a>
								<a href="#" target="_blank" class="gdlr-core-social-network-icon" title="instagram">
									<i class="fa fa-instagram" ></i>
								</a>
							</div>
						</div>
					</div>
				</div>
            </footer>
