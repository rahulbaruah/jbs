<div class="container">
                    
    <div class="kingster-item-pdlr">
        <h4 style="margin-bottom:0;">Jettwings Story</h4>
    </div>
    
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-white" style="padding-top:0;padding-left: 0;margin-left: 20px;">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">About</a></li>
        <li class="breadcrumb-item active" aria-current="page">Our Motto</li>
      </ol>
    </nav>
    <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #ec2f45; ;border-bottom-width: 2px ;"></div>
    </div>
    <div class="row">
        <div class="col-md-4">
            
            <div class="left-sub-box">
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
                <div class="kingster-sidebar-area kingster-item-pdlr" style="padding-top:0">
                                
                                <div id="text-5" class="widget widget_text kingster-widget" style="background-color: #f7f7f7">
                                        <h3 class="kingster-widget-title">Lorem ipsum</h3><span class="clear"></span>
                                        <div class="textwidget">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat. Nunc aliquet bibendum enim facilisis gravida. Nisl nunc mi ipsum faucibus vitae aliquet nec ullamcorper.</p>
                                        </div>
                                </div>
                </div>
                
            </div>
            
            <div class="left-sub-box">
                <img src="https://via.placeholder.com/450x530/ec2f45/ffffff"></img>
            </div>
        </div>
        <div class="col-md-8">
            
            <div class="right-sub-box">
            <h3>Our Vision</h3>
            <p><b>Learning for all</b> — with this vision in mind, we envisage transforming JBS into a citadel of new-age education with an open collaborative approach where the brightest of the minds find an epoch of new learning experience. Experience with us a platform of shared learning with ethical practices to spearhead the competition and transform individual life.</p>
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Our Values</h3>
            <p>If there’s something that kept us together all these years is our values.</p>
            <p>A set of sacrosanct principles that transmits the same DNA in our students and make them equally responsible towards their community in many ways such as:</p>
            
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-icon-list-item gdlr-core-item-pdlr gdlr-core-item-pdb clearfix " style="padding-bottom: 25px ;">
                                                    <ul>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Through an action-driven learning methodology
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                By practicing multi-cultural engagements
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Making efforts to restore and retain environmental elements
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Understanding multi-ethnicity work ambiance and creating respectful cohabitation at workplace
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Through a shared sense of responsibility towards our communities
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Seeking thought leadership in the market space
                                                            </span></div>
                                                        </li>
                                                        <li class=" gdlr-core-skin-divider" style="margin-bottom: 22px ;"><span class="gdlr-core-icon-list-icon-wrap" style="margin-top: 5px ;"><i class="gdlr-core-icon-list-icon fa fa-dot-circle-o" style="color: #000000 ;font-size: 18px ;width: 18px ;" ></i></span>
                                                            <div class="gdlr-core-icon-list-content-wrap"><span class="gdlr-core-icon-list-content" style="font-size: 17px ;">
                                                                Inspiring ethical practices
                                                            </span></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
            
            </div>
            
            <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #000000; ;border-bottom-width: 1px ;"></div>
            </div>
            
            <div class="right-sub-box">
            <h3>Our Philosophy</h3>
            <p>As we embark on a journey of shared learning, we pledge to make certain commitments through:</p>
            
            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-accordion-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-accordion-style-background-title-icon gdlr-core-left-align gdlr-core-icon-pos-right">
                                                    <div class="gdlr-core-accordion-item-tab clearfix  gdlr-core-active">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Global citizenship</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>Diversity is an integral part of our learning culture; JBS, with its variegated colors of multi-cultural touch, welcomes youths from a diversified community. It indoctrinates tolerance towards a multi-ethnicity and promotes cultural confluence through various community engagements within the campus and prepares youths to be a true global citizen.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Environmental responsibility</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>Environmental sustainability is a global concern. We have crafted a learning mechanism that keeps in its module many environmental concerns and encourages youths to be more sensitive towards their needs. Further, we ensure that each individual understands one’s own share of responsibility and contributes to building a community of socio-environmental custodians</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Transformative education</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <p>What education is if not for transformation—with this thought in mind, we roll out a multi-disciplinary learning module across industry segments in different verticals. With international exposure, we make our students believe that they can cut a significant slice in the market space in terms of value addition and innovation.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">The Hotel & Travel School – a concept by Jettwings</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <ol>
                                                                    <li>School of Hotel Management</li>
                                                                    <li>School Tourism Studies</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="gdlr-core-accordion-item-tab clearfix ">
                                                        <div class="gdlr-core-accordion-item-icon gdlr-core-js gdlr-core-skin-icon "></div>
                                                        <div class="gdlr-core-accordion-item-content-wrapper">
                                                            <h4 class="gdlr-core-accordion-item-title gdlr-core-js  gdlr-core-skin-e-background gdlr-core-skin-e-content">Jettwings Institute of Fashion, Design & Architecture (JiFDA)</h4>
                                                            <div class="gdlr-core-accordion-item-content">
                                                                <ol>
                                                                    <li>School of Fashion  & Textile Technology</li>
                                                                    <li>School of Interior Design</li>
                                                                    <li>School of Architecture</li>
                                                                    <li>School of Design Communications & Advertising</li>
                                                                </ol>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
            
            </div>
            
        </div>
    </div>

</div>
                    
<div class="gdlr-core-pbf-wrapper " style="padding: 65px 0px 60px 0px;margin-top:50px">
                        <div class="gdlr-core-pbf-background-wrap" style="background-color: #eeeeee ;"></div>
                        <div class="gdlr-core-pbf-wrapper-content gdlr-core-js">
                            <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 45px 0px 0px 0px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 20px ;">
                                                    <div class="gdlr-core-text-box-item-content" style="font-size: 23px ;text-transform: none ;color: #000000 ;">
                                                        <p>Through JBS we would like to promote a new-aged education that will result in a special breed of professionals from a multi-cultural ethnicity and prepare them for diversified global work culture. 
<br/>
<b>So, come be a part of a richer learning experience</b>
</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align"><a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #ec2f45 ;"><span class="gdlr-core-content" >Apply Now</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30" id="gdlr-core-column-1">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="margin: -123px 0px 0px 0px;padding: 0px 0px 0px 40px;">
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js ">
                                            <div class="gdlr-core-pbf-element">
                                                <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-center-align">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="http://via.placeholder.com/400x257/000000/ffffff" width="700" height="450"  alt="" /></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>