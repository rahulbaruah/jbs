<div class="kingster-top-bar">
                <div class="kingster-top-bar-background"></div>
                <div class="kingster-top-bar-container kingster-container ">
                    <div class="kingster-top-bar-container-inner clearfix">
                        <div class="kingster-top-bar-left kingster-item-pdlr">
                            <i class="fa fa-phone" style="font-size: 15px ;margin-right: 6px ;"></i> (+91) 99540-90900
                            <i class="fa fa-envelope" style="font-size: 15px ;margin-left: 18px ;margin-right: 6px ;"></i> info@jettwingsbschool.com
                        </div>
                        <div class="kingster-top-bar-right kingster-item-pdlr">
                            <ul id="kingster-top-bar-menu" class="sf-menu kingster-top-bar-menu kingster-top-bar-right-menu">
                                <li class="menu-item kingster-normal-menu"><a href="#">Scholarship</a></li>
                                <li class="menu-item kingster-normal-menu"><a href="#">Admission Helpline</a></li>
                                <li class="menu-item kingster-normal-menu"><a href="#">Download Brochure</a></li>
                            </ul>
                            <div class="kingster-top-bar-right-social"></div><a class="kingster-top-bar-right-button" href="#" target="_blank">APPLY ONLINE</a></div>
                    </div>
                </div>
</div>